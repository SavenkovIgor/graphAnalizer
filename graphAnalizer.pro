#-------------------------------------------------
#
# Project created by QtCreator 2012-07-07T01:38:30
#
#-------------------------------------------------

QT       += core gui opengl

TARGET = graphAnalizer
TEMPLATE = app


SOURCES += main.cpp\
        graphanalizer.cpp \
    physobject.cpp

HEADERS  += graphanalizer.h \
    physobject.h

FORMS    += graphanalizer.ui
