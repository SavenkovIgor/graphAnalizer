#ifndef GRAPHANALIZER_H
#define GRAPHANALIZER_H

#include <QMainWindow>
#include <qgl.h>

namespace Ui {
class graphAnalizer;
}

class graphAnalizer : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit graphAnalizer(QWidget *parent = 0);
    ~graphAnalizer();
    
private:
    Ui::graphAnalizer *ui;
};

#endif // GRAPHANALIZER_H
