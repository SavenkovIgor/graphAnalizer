#ifndef PHYSOBJECT_H
#define PHYSOBJECT_H

#include <QObject>
#include <QPointF>
#include <QVector2D>

#define stdVisc 0.8

class physObject : public QObject
{
    Q_OBJECT
public:
    explicit physObject(QObject *parent = 0);


    QPointF pos;
    double mass;
    QVector2D speed;
    double viscosity;

    void setForce(QVector2D force);
    void setViscosity(double vis);
    virtual void worldStep();
    virtual void drawYouself(QPainter &pnt);



signals:
    
public slots:
    
};

#endif // PHYSOBJECT_H
