#include "physobject.h"

physObject::physObject(QObject *parent) :
    QObject(parent)
{
}

void physObject::setForce(QVector2D force)
{
    force /= mass;
    speed +=force;
}

void physObject::setViscosity(double vis)
{
    viscosity = vis;
    if(vis < 0)
        viscosity = stdVisc;
}

